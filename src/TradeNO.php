<?php
namespace Ajax\Generate;

class TradeNO
{
    private $prefix = '';

    /**
     * 根据时间生成不重复的数字
     * @return string
     */
    public function createTradeNo()
    {
        return $this->prefix . date('YmdHis') . substr(microtime(), 2, 6) . sprintf('%04d', rand(0, 9999));
    }

    /**
     * 设置前缀
     * @param $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * 生成随机字符串
     * @param $len
     * @return string
     */
    public function rand($len)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $string = time();
        for (; $len >= 1; $len--) {
            $position = rand() % strlen($chars);
            $position2 = rand() % strlen($string);
            $string = substr_replace($string, substr($chars, $position, 1), $position2, 0);
        }
        return $this->prefix . $string;
    }
}